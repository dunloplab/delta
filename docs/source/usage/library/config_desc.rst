Configuration
=============

.. note::

    This page is a high-level overview of the :class:`delta.config` module of
    DeLTA, and you can find more technical explanations by clicking on any
    link.

DeLTA can work on different kinds of images, mothermachine and 2D (agar pads).
Different steps and networks are used for that: this configuration is described
by a :class:`delta.config.Config` object.

The only places where this object needs to be explicitly manipulated is at the
creation of a :class:`delta.pipeline.Pipeline`, a
:class:`delta.pipeline.Position` or a :class:`delta.pipeline.ROI`.

DeLTA provides two base configurations, one for each setting (mothermachine and
2D), available with the method :class:`delta.config.Config.default`.  They are
good to use right away, but you would typically modify them if you want to use
your own deep learning models instead of the ones included with DeLTA.

.. note::

    Any change in the config option ``memory_growth_limit`` will not be taken
    into account unless the method
    :class:`delta.config.Config.apply_tensorflow_config` is explicitly called.
    The default values are usually good, so you probably don't need to care
    about this, unless you know that you do.

Deep learning model selection
-----------------------------

The :class:`delta.config.Config` knows where to find the deep learning models
included with DeLTA, for example ``config.model("segmentation")`` returns the
segmentation model as a keras object, should you ever need to access it
directly.

Under the scenes, it downloads it and caches it on the disk if you don't
already have it (go to :class:`delta.assets` to know how this works).  This is
what happens when a model-related attribute of ``config`` is None.  But if you
trained your own model, you will need to provide its location to the config,
for example as following:

.. code:: python

    from delta.config import Config

    config = Config.default("mothermachine")
    # here, ``config.model("rois")`` would download and return the default model
    config.model_file_rois = "D:/data/delta_cache/unet_momachambers_seg.keras"
    # here, ``config.model("rois")`` would now load your model
    # but you can also of course feed this config to a ``Pipeline``

Saving and using a configuration file
-------------------------------------

If you want to modify a configuration object, you can always do it
programmatically by accessing its attributes, as described on the documentation
page of :class:`delta.config.Config`, for example:

.. code:: python

    from delta.config import Config

    config = Config.default("mothermachine")
    config.target_size_seg = (1024, 1024)
    config.min_cell_area = 0

But you might also want to save the config object as a configuration file, and
modify it with a text editor, or save it for reusing, archiving or other
purposes.

This is done with the method :class:`delta.config.Config.write` which takes as
argument the path to the json file where you want the configuration written:

.. code:: python

    config.write("my_custom_config.json")

To read later from this file, use the method :class:`delta.config.Config.read`:

.. code:: python

    config = delta.config.Config.read("my_custom_config.json")
