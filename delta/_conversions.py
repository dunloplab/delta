"""Various conversions to save objects to disk."""

import ast

import cv2
import numpy as np
import numpy.typing as npt
import xarray as xr

import delta
from delta import imgops, utils
from delta.config import Config
from delta.lineage import Pole
from delta.model import hash_model


def _roi_to_xarray(roi: "delta.pipeline.ROI") -> xr.Dataset:
    # DataArray coordinates
    frames = np.arange(len(roi.img_stack), dtype=np.int32) + roi.first_frame
    y_orig = np.arange(roi.img_stack[0].shape[0], dtype=np.int16)
    x_orig = np.arange(roi.img_stack[0].shape[1], dtype=np.int16)
    tyx_orig = {"frame": frames, "y_orig": y_orig, "x_orig": x_orig}
    y_resized = np.arange(roi.seg_stack[0].shape[0], dtype=np.int16)
    x_resized = np.arange(roi.seg_stack[0].shape[1], dtype=np.int16)
    tyx_resized = {"frame": frames, "y_resized": y_resized, "x_resized": x_resized}
    cells = np.array(list(roi.lineage.cells.keys()), dtype=np.uint16)
    ct = {"cell": cells, "frame": frames}
    nb_channels = roi.fluo_stack.shape[1]
    channels = np.arange(nb_channels, dtype=np.uint8)
    edge = ["-x", "+x", "-y", "+y"]

    # Image stacks
    img_stack = xr.DataArray(
        roi.img_stack, dims=("frame", "y_orig", "x_orig"), coords=tyx_orig
    )
    fluo_stack = xr.DataArray(
        roi.fluo_stack,
        dims=("frame", "channel", "y_orig", "x_orig"),
        coords=tyx_orig | {"channel": channels},
    )
    seg_stack = xr.DataArray(
        roi.seg_stack, dims=("frame", "y_resized", "x_resized"), coords=tyx_resized
    ).astype(bool)
    label_stack = xr.DataArray(
        roi.label_stack, dims=("frame", "y_orig", "x_orig"), coords=tyx_orig
    )

    # Lineage (first define numpy arrays, then transform them into DataArrays)
    nmother = np.zeros((len(cells),), dtype=np.uint16)
    ndaughters = np.zeros((len(cells), len(frames)), dtype=np.uint16)
    nnew_pole = np.zeros((len(cells), len(frames), 2), dtype=np.int16)
    nold_pole = np.zeros((len(cells), len(frames), 2), dtype=np.int16)
    nedges = np.zeros((len(cells), len(frames), 4), dtype=bool)
    nscalars = {
        "length": np.full((len(cells), len(frames)), np.nan, dtype=np.float32),
        "width": np.full((len(cells), len(frames)), np.nan, dtype=np.float32),
        "area": np.full((len(cells), len(frames)), np.nan, dtype=np.float32),
        "perimeter": np.full((len(cells), len(frames)), np.nan, dtype=np.float32),
        "growthrate_length": np.full(
            (len(cells), len(frames)), np.nan, dtype=np.float32
        ),
        "growthrate_area": np.full((len(cells), len(frames)), np.nan, dtype=np.float32),
    }
    nfluo = np.full((len(cells), len(frames), len(channels)), np.nan, dtype=np.float32)
    for icell, cellid in enumerate(cells):
        cell = roi.lineage.cells[cellid]
        nmother[icell] = cell.motherid or 0
        for frame in cell.frames:
            features = cell.features(frame)
            ndaughters[icell, frame - roi.first_frame] = cell.daughterid(frame) or 0
            nnew_pole[icell, frame - roi.first_frame] = features.new_pole
            nold_pole[icell, frame - roi.first_frame] = features.old_pole
            for arg, narg in nscalars.items():
                narg[icell, frame - roi.first_frame] = getattr(features, arg)
            nfluo[icell, frame - roi.first_frame] = features.fluo
            for iedge, edg in enumerate(edge):
                nedges[icell, frame - roi.first_frame, iedge] = edg in features.edges

    # Transform lineage information into DataArrays
    mother = xr.DataArray(nmother, dims=("cell",), coords={"cell": cells})
    daughters = xr.DataArray(ndaughters, dims=("cell", "frame"), coords=ct)
    ctyx = ct | {"yx": ["y", "x"]}
    new_pole = xr.DataArray(nnew_pole, dims=("cell", "frame", "yx"), coords=ctyx)
    old_pole = xr.DataArray(nold_pole, dims=("cell", "frame", "yx"), coords=ctyx)
    scalars = {
        arg: xr.DataArray(narg, dims=("cell", "frame"), coords=ct)
        for arg, narg in nscalars.items()
    }
    fluo = xr.DataArray(
        nfluo, dims=("cell", "frame", "channel"), coords=ct | {"channel": channels}
    )
    edges = xr.DataArray(
        nedges, dims=("cell", "frame", "edge"), coords=ct | {"edge": edge}
    )
    first_frame = xr.DataArray(
        np.array(
            [roi.lineage.cells[cellid].first_frame for cellid in cells], dtype=np.int32
        ),
        dims=("cell",),
        coords={"cell": cells},
    )
    last_frame = xr.DataArray(
        np.array(
            [roi.lineage.cells[cellid].last_frame for cellid in cells], dtype=np.int32
        ),
        dims=("cell",),
        coords={"cell": cells},
    )

    # Create Dataset
    model_hashes = {
        f"{model}_model_hash": hash_model(roi.config.model(model))
        for model in roi.config.models
    }
    dataset = xr.Dataset(
        data_vars={
            "img_stack": img_stack,
            "fluo_stack": fluo_stack,
            "seg_stack": seg_stack,
            "label_stack": label_stack,
            "mother": mother,
            "daughter": daughters,
            "new_pole": new_pole,
            "old_pole": old_pole,
            "edges": edges,
            "fluo": fluo,
            "first_frame": first_frame,
            "last_frame": last_frame,
        }
        | scalars,
        attrs={
            "roi_nb": roi.roi_nb,
            "box": str(roi.box.__dict__),
            "scaling": roi.scaling,
            "config": str(roi.config.sanitize()),
            "DeLTA_version": delta.__version__,
            "file_format_version": "0.1.0",
        }
        | model_hashes,
    )
    return dataset


def _xarray_to_roi(dataset: xr.Dataset) -> "delta.pipeline.ROI":
    box = imgops.CroppingBox(**ast.literal_eval(dataset.attrs["box"]))
    config = Config(**ast.literal_eval(dataset.attrs["config"]))
    img_stack = list(dataset.img_stack.to_numpy())
    fluo_stack = list(dataset.fluo_stack.to_numpy())
    roi = delta.pipeline.ROI(
        img_stack=img_stack,
        fluo_stack=fluo_stack,
        roi_nb=dataset.attrs["roi_nb"],
        first_frame=dataset.frame[0].to_numpy() if len(dataset.frame) > 0 else 0,
        box=box,
        config=config,
    )
    roi.scaling = tuple(dataset.attrs["scaling"])
    roi.seg_stack = list(dataset.seg_stack.to_numpy().astype(np.uint8))
    roi.label_stack = list(dataset.label_stack.to_numpy())
    cells = {}
    for cellid in [int(cellid) for cellid in dataset.cell]:
        xrcell = dataset.sel(cell=cellid)
        mother = int(xrcell.mother)
        frames = np.arange(xrcell.first_frame, xrcell.last_frame + 1)
        data = xrcell.sel(frame=frames)
        daughters = data.daughter.to_numpy()
        edges = [
            "".join(
                str(e.to_numpy()) if data.edges.sel(frame=frame, edge=e) else ""
                for e in data.edge
            )
            for frame in data.frame
        ]
        cells[cellid] = delta.lineage.Cell(
            motherid=mother if mother > 0 else None,
            first_frame=frames[0],
            _daughterids=[did if did > 0 else None for did in daughters],
            _features=[
                delta.lineage.CellFeatures(
                    new_pole=new_pole,
                    old_pole=old_pole,
                    length=length,
                    width=width,
                    area=area,
                    perimeter=perimeter,
                    fluo=fluo,
                    edges=edges,
                    growthrate_length=gr_l,
                    growthrate_area=gr_a,
                )
                for (
                    new_pole,
                    old_pole,
                    length,
                    width,
                    area,
                    perimeter,
                    fluo,
                    edges,
                    gr_l,
                    gr_a,
                ) in zip(
                    data.new_pole.to_numpy(),
                    data.old_pole.to_numpy(),
                    data.length.to_numpy(),
                    data.width.to_numpy(),
                    data.area.to_numpy(),
                    data.perimeter.to_numpy(),
                    data.fluo.to_numpy(),
                    edges,
                    data.growthrate_length.to_numpy(),
                    data.growthrate_area.to_numpy(),
                    strict=True,
                )
            ],
        )
    roi.lineage.cells = cells
    return roi


def _position_to_movie(
    position: "delta.pipeline.Position",
    reader: delta.utils.XPReader,
    frames: range | None = None,
    *,
    with_labels: bool = False,
) -> list[npt.NDArray[np.uint8]]:
    # Re-read trans frames:
    trans_images = reader.images(
        position=position.position_nb,
        channels=reader.channels[0],
        frames=frames,
        rotate=position.rotate,
    )
    if frames is None:
        frames = reader.frames
    trans_images = trans_images[:, 0]

    if position.config.drift_correction:
        trans_images = imgops.correct_drift(trans_images, position.drift_values)

    # Fix brightness
    trans_images = (trans_images - trans_images.min()) / np.ptp(trans_images)

    movie = []

    # Run through frames, compile movie:
    for frame, init_trans_frame in zip(frames, trans_images, strict=True):
        trans_frame = init_trans_frame

        # RGB-ify:
        trans_frame = np.repeat(trans_frame[:, :, np.newaxis], 3, axis=-1)

        # Add frame number text:
        trans_frame = cv2.putText(
            trans_frame,
            text=f"frame {frame:06d}",
            org=(int(trans_frame.shape[0] * 0.05), int(trans_frame.shape[0] * 0.97)),
            fontFace=cv2.FONT_HERSHEY_SIMPLEX,
            fontScale=1,
            color=(1, 1, 1, 1),
            thickness=2,
        )

        for roi in position.rois:
            # Get chamber-specific variables:
            colors = utils.random_colors(
                list(roi.lineage.cells.keys()), seed=roi.roi_nb
            )
            labels = roi.get_labels(frame)
            cellids, contours = utils.cells_in_frame(labels, return_contours=True)

            xtl, ytl = (roi.box.xtl, roi.box.ytl)

            # Run through cells in labelled frame:
            for icell, cellid in enumerate(cellids):
                cell = roi.lineage.cells[cellid]
                # Draw contours:
                trans_frame = cv2.drawContours(
                    trans_frame,
                    contours,
                    icell,
                    color=colors[cellid],
                    thickness=1,
                    offset=(xtl, ytl),
                )

                if with_labels:
                    cell_y, cell_x = np.where(labels == cellid)
                    trans_frame = cv2.putText(
                        trans_frame,
                        text=str(cellid),
                        org=(
                            int(np.mean(cell_x) + xtl + 5),
                            int(np.mean(cell_y) + ytl + 5),
                        ),
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=0.5,
                        color=(1, 1, 1, 1),
                        thickness=1,
                    )

                # Draw poles:
                oldpole = cell.features(frame).old_pole
                assert isinstance(oldpole, np.ndarray)  # for mypy
                trans_frame = _draw_pole(
                    trans_frame,
                    contours[icell],
                    oldpole,
                    (xtl, ytl),
                    color=colors[cellid],
                )

                daughter = cell.daughterid(frame)
                bornago = frame - cell.first_frame
                mother = cell.motherid

                if daughter is None and (bornago > 0 or mother is None):
                    newpole = cell.features(frame).new_pole
                    trans_frame = _draw_pole(
                        trans_frame,
                        contours[icell],
                        newpole,
                        (xtl, ytl),
                        color=(1.0, 1.0, 1.0),
                    )

                # Plot division arrow:
                if daughter is not None:
                    newpole = cell.features(frame).new_pole
                    daupole = roi.lineage.cells[daughter].features(frame).new_pole
                    # Plot arrow:
                    trans_frame = cv2.arrowedLine(
                        trans_frame,
                        (newpole[1] + xtl, newpole[0] + ytl),
                        (daupole[1] + xtl, daupole[0] + ytl),
                        color=(1, 1, 1),
                        thickness=1,
                    )

        # Add to movie array:
        movie += [imgops.to_integer_values(trans_frame, np.uint8)]

    return movie


def _draw_pole(
    image: npt.NDArray[np.float32],
    contour: npt.NDArray[np.int32],
    pole: Pole,
    offset: tuple[int, int],
    color: tuple[float, float, float],
    width: int = 7,
) -> npt.NDArray[np.float32]:
    contour = contour[:, 0, :]
    pole_ind = np.argmin(np.sum(np.abs(contour - pole[::-1]), axis=1))
    pole_cnt = contour[np.arange(pole_ind - width, pole_ind + width) % len(contour)]
    return cv2.drawContours(image, [pole_cnt[:, None, :]], -1, color, -1, offset=offset)


def _position_to_labels(
    position: "delta.pipeline.Position",
    frames: range | None = None,
    *,
    undo_corrections: bool = True,
) -> list[imgops.Labels]:
    if frames is None:
        frames = range(
            position.rois[0].first_frame,
            position.rois[0].first_frame + len(position.rois[0].label_stack),
        )

    labels = []
    for frame in frames:
        labelled = np.zeros(position.shape, dtype=np.uint16)

        for roi in position.rois:
            roi.box.patch(labelled, roi.get_labels(frame))

        # Undo drift and rotation corrections:
        if undo_corrections:
            if position.config.drift_correction:
                drift = position.drift_values[frame - position.rois[0].first_frame]
                drift = (-drift[0], -drift[1])
                labelled = imgops.correct_drift(labelled[None], [drift])[0]
            if position.config.rotation_correction:
                labelled = imgops.rotate_image(labelled, -position.rotate)

        labels.append(labelled)

    return labels
