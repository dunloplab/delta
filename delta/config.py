"""Define, read and write DeLTA's configuration."""

import json
import logging
from dataclasses import asdict, dataclass
from pathlib import Path
from typing import Any, Literal

import keras

from delta import assets, utils

LOGGER = logging.getLogger(__name__)

MODEL = Literal["rois", "seg", "track"]

PRESETS = Literal["2D", "mothermachine"]


def _type_for_config(key: str, value: str) -> Path | bool | int | float:
    """Convert the value to the proper type."""
    if key in {
        "model_file_rois",
        "model_file_seg",
        "model_file_track",
        "training_set_rois",
        "training_set_seg",
        "training_set_track",
        "eval_movie",
    }:
        return Path(value)
    if key in {
        "rotation_correction",
        "drift_correction",
        "whole_frame_drift",
        "crop_windows",
    }:
        if value.lower() in {"true", "1"}:
            return True
        if value.lower() in {"false", "0"}:
            return False
        raise ValueError
    if key in {
        "min_roi_area",
        "min_cell_area",
        "memory_growth_limit",
        "pipeline_seg_batch",
        "pipeline_track_batch",
        "pipeline_chunk_size",
        "number_of_cores",
    }:
        return int(value)
    if key == "tolerable_resizing_rois":
        return float(value)
    raise NotImplementedError


@dataclass
class Config:
    """Configuration class containing DeLTA parameters."""

    presets: PRESETS
    """Type of analysis: can be '2D' or 'mothermachine'."""
    models: tuple[MODEL, ...]
    """Which models need to be loaded ('rois', 'seg' or 'track')."""
    model_file_rois: Path | None
    """Path to the model file for ROI segmentation."""
    model_file_seg: Path | None
    """Path to the model file for cell segmentation."""
    model_file_track: Path | None
    """Path to the model file for cell tracking."""
    target_size_rois: tuple[int, int]
    """Input size of the ROI segmentation model."""
    target_size_seg: tuple[int, int]
    """Input size of the cell segmentation model."""
    target_size_track: tuple[int, int]
    """Input size of the cell tracking model."""
    training_set_rois: Path | None
    """Path to the ROI segmentation training set."""
    training_set_seg: Path | None
    """Path to the cell segmentation training set."""
    training_set_track: Path | None
    """Path to the cell tracking training set."""
    eval_movie: Path | None
    """Path to the evaluation movie."""
    rotation_correction: bool
    """Whether or not to correct for image rotation (mothermachine only)."""
    drift_correction: bool
    """Whether or not to correct for drift over time (mothermachine only)."""
    whole_frame_drift: bool
    """If correcting for drift, use the entire frame instead of the region
    above the chambers."""
    crop_windows: bool
    """If True, crop input images into windows of size target_size_seg for
    segmentation, otherwise resize them."""
    min_roi_area: int
    """Minimum area of detected ROIs in pixels (mothermachine only)."""
    min_cell_area: int
    """Minimum area of detected cells in pixels."""
    memory_growth_limit: int | None
    """If running into OOM issues or having troupble with CuDNN loading, try
    setting this to a value in MB: 1024, 2048, etc."""
    pipeline_seg_batch: int
    """If running into OOM issues during segmentation with the pipeline, try
    lowering this value.  You can also increase it to improve speed."""
    pipeline_track_batch: int
    """If running into OOM issues during segmentation with the pipeline, try
    lowering this value.  You can also increase it to improve speed."""
    pipeline_chunk_size: int
    """If running into OOM issues during segmentation with the pipeline, try
    lowering this value.  You can also increase it to improve speed."""
    number_of_cores: int | None
    """This will limit the number of cores TensorFlow can use. Default value
    of this will have TensorFlow determine the number of cores."""
    tolerable_resizing_rois: float
    """Maximum tolerable resizing factor of the template for ROI
    identification.  On either axes (x and y), resizing only happens if
    ``ref < target * (tolerable_resizing_rois + 1)`` where ``ref`` is the size
    of the reference image on one axis, and ``target`` is the input size of the
    neural network defined in the config as ``target_size_rois`` on the same
    axis.  If the reference image is larger than that, it is cropped."""

    @classmethod
    def default(cls, presets: PRESETS) -> "Config":
        """
        Return the default config for this preset.

        Parameters
        ----------
        presets : PRESETS
            Can be "2D" or "mothermachine".

        Returns
        -------
        config : Config
            Config object.
        """
        default_config = {
            "2D": _DEFAULT_CONFIG_2D,
            "mothermachine": _DEFAULT_CONFIG_MOTHERMACHINE,
        }[presets]
        return cls(**default_config.__dict__)

    @classmethod
    def read(cls, path: str | Path) -> "Config":
        """
        Read a configuration file.

        Parameters
        ----------
        path : str | Path
            Path to specific configuration file.

        Returns
        -------
        config : Config
            Loaded config object.

        """
        try:
            return cls(**read_json(Path(path)))
        except TypeError as err:
            error_msg = (
                "The config file has too many or is missing some parameters. "
                "This is most likely because the config file was generated for "
                "an earlier version of DeLTA. Please update your config based "
                "on the current one, detailed in `delta.config.Config`."
            )
            raise ValueError(error_msg) from err

    def update(self, key: str, value: Path | float | bool | str) -> None:  # noqa: FBT001
        """
        Update in place the configuration with the given key and value.

        Parameters
        ----------
        key : str
            Config key to update.
        value : Any
            Value for the given key.
        """
        if key not in asdict(self):
            error_msg = f"{key} is not a valid configuration option"
            raise ValueError(error_msg)
        typed_value = _type_for_config(key, value) if isinstance(value, str) else value
        setattr(self, key, typed_value)

    def write(self, path: str | Path) -> None:
        """
        Write a configuration file.

        Parameters
        ----------
        path : str | Path
            Path to specific configuration file.

        """
        write_json(self, Path(path))

    def compare(self, other: object, level: int = 0) -> list | None:  # type: ignore[type-arg]
        """Return or print the list of differences between two Config objects."""
        diffs: list[str | list] = []  # type: ignore[type-arg]
        if not isinstance(other, Config):
            diffs.append(utils.color_diff("", "Config", type(other)))
        else:
            diffs.append("Config")
            diffs += [
                utils.color_diff(f"{key}: ", self.__dict__[key], other.__dict__[key])
                for key in self.__dict__ | other.__dict__
                if self.__dict__[key] != other.__dict__[key]
            ]
        if level == 0:
            utils.print_diffs(diffs)
            return None
        return diffs

    def training_set_path(self, model: MODEL) -> Path:
        """
        Return the path of the training set for the specified model.

        If necessary, we download it first and cache it under a directory
        than can be specified with the environment variable `DELTA_ASSETS_CACHE`.

        Parameters
        ----------
        model : str
            Type of training set: `"rois"`, `"seg"` or `"track"`.

        Returns
        -------
        path : Path
            Path of the training set on the disk.
        """
        config_path: Path | None = self.__dict__[f"training_set_{model}"]
        if config_path:
            return config_path
        path = assets.download_training_set(self.presets, model)
        if self.presets == "mothermachine" and model == "track":
            return path / "train_multisets"
        return path

    def demo_movie_path(self) -> Path:
        """
        Return the path of the demo movie for the specified presets.

        If necessary, we download it first and cache it uder a directory
        than can be specified with the environment variable `DELTA_ASSETS_CACHE`.

        Returns
        -------
        path : Path
            Path of the training set on the disk.
        """
        if self.eval_movie:
            return self.eval_movie
        return assets.download_demo_movie(self.presets)

    def model(self, model: MODEL) -> keras.Model:
        """
        Return the keras model for the specified preset and model type.

        If the variable "model_file_(rois|seg|track)" is non-empty in the
        configuration, it is assumed to be the filename of the model.  If this
        variable is empty, we download the default model of DeLTA and cache it
        under a directory that can be optionally specified with the environment
        variable `DELTA_ASSETS_CACHE`.

        Parameters
        ----------
        model : str
            Model type ("rois", "segmentation", or "tracking").

        Returns
        -------
        model : keras.Model
            The required model.
        """
        if self.__dict__[f"model_file_{model}"]:
            model_path = self.__dict__[f"model_file_{model}"]
        else:
            model_path = assets.download_model(self.presets, model)
        keras_model = keras.models.load_model(model_path, compile=False)

        # In order to be able to use the old models that had a sigmoid as a final activation
        keras_model.layers[-1].activation = keras.activations.linear

        return keras_model

    def apply_backend_config(self) -> None:
        """Apply backend-relevant parts of the config object."""
        if keras.src.backend.config.backend() == "tensorflow":
            self._apply_tensorflow_config()

        if keras.src.backend.config.backend() == "torch":
            self._apply_torch_config()

        if keras.src.backend.config.backend() == "jax":
            self._apply_jax_config()

    def _apply_tensorflow_config(self) -> None:
        """
        Apply the tensorflow-relevant parts of the config object.

        These are currently the ``TF_CPP_MIN_LOG_LEVEL`` environment variable,
        and the ``memory_growth_limit`` parameter.

        TensorFlow will also determine the number of cores if not set in the config
        """
        import tensorflow as tf  # noqa: PLC0415

        # If running into OOM issues or having trouble with cuDNN loading, try setting
        # memory_growth_limit to a value in MB: (eg 1024, 2048...)
        gpus = tf.config.experimental.list_physical_devices("GPU")
        if self.memory_growth_limit is not None and gpus:
            # Restrict TensorFlow to only allocate 1GB of memory on the first GPU
            try:
                vdconf = tf.config.experimental.VirtualDeviceConfiguration(
                    memory_limit=self.memory_growth_limit
                )
                tf.config.experimental.set_virtual_device_configuration(
                    gpus[0], [vdconf]
                )
                logical_gpus = tf.config.experimental.list_logical_devices("GPU")
                LOGGER.info(
                    "%d physical GPUs, %d logical GPUs",
                    len(gpus),
                    len(logical_gpus),
                )
            except RuntimeError:
                error_msg = "Virtual devices must be set before GPUs are initialized"
                LOGGER.exception(error_msg)

        # TensorFlow will determine the max number of cores if not set in the config
        tf.config.threading.set_inter_op_parallelism_threads(self.number_of_cores)
        tf.config.threading.set_intra_op_parallelism_threads(self.number_of_cores)

    def _apply_torch_config(self) -> None:
        """
        Apply the torch-relevant parts of the config object.

        This currently does nothing.
        """

    def _apply_jax_config(self) -> None:
        """
        Apply the jax-relevant parts of the config object.

        This currently does nothing.
        """

    def sanitize(self) -> dict[str, str | tuple[int, int] | bool | int | float]:
        """Return a serializable version of the config."""
        return {
            key: str(value) if isinstance(value, Path) else value
            for key, value in self.__dict__.items()
        }

    def __str__(self) -> str:
        """Return an informal description of the object."""
        s = ["DeLTA config"]
        for k, v in self.__dict__.items():
            if v is not None:
                s.append(f" ├─ {k}: {v}")
        s.append(" └─ All other parameters are None.")
        return "\n".join(s)


###
# IMPORTANT: Do not change the default parameters below.
# Update the .yml files instead if necessary.
###

_DEFAULT_CONFIG_2D = Config(
    presets="2D",
    models=("seg", "track"),
    model_file_rois=None,
    model_file_seg=None,
    model_file_track=None,
    target_size_rois=(512, 512),
    target_size_seg=(512, 512),
    target_size_track=(256, 256),
    training_set_rois=None,
    training_set_seg=None,
    training_set_track=None,
    eval_movie=None,
    rotation_correction=False,
    drift_correction=False,
    crop_windows=True,
    min_roi_area=500,
    whole_frame_drift=False,
    min_cell_area=20,
    memory_growth_limit=None,
    pipeline_seg_batch=1,
    pipeline_track_batch=1,
    pipeline_chunk_size=64,
    number_of_cores=None,
    tolerable_resizing_rois=4.0,
)
"""Base configuration for 2D movies (agar pads)."""

_DEFAULT_CONFIG_MOTHERMACHINE = Config(
    presets="mothermachine",
    models=("rois", "seg", "track"),
    model_file_rois=None,
    model_file_seg=None,
    model_file_track=None,
    target_size_rois=(512, 512),
    target_size_seg=(256, 32),
    target_size_track=(256, 32),
    training_set_rois=None,
    training_set_seg=None,
    training_set_track=None,
    eval_movie=None,
    rotation_correction=True,
    drift_correction=True,
    crop_windows=False,
    min_roi_area=500,
    whole_frame_drift=False,
    min_cell_area=20,
    memory_growth_limit=None,
    pipeline_seg_batch=1,
    pipeline_track_batch=64,
    pipeline_chunk_size=64,
    number_of_cores=None,
    tolerable_resizing_rois=4.0,
)
"""Base configuration for mothermachine movies."""


def read_json(path: Path) -> dict[str, Any]:
    """
    Read a JSON file containing a configuration.

    This function converts the lists to tuples and the paths to ``Path``s.

    Parameters
    ----------
    path : Path
        Path of the file to read.

    Returns
    -------
    variables : dict[str, Any]
        Dictionary representing the JSON.
    """
    LOGGER.info("Loading configuration from: %s", path)
    # Load file:
    with path.open("r") as file:
        variables: dict[str, Any] = json.load(file)

    # Type cast:
    for key, value in variables.items():
        if isinstance(value, list):
            # Always use tuples, not lists in config
            variables[key] = tuple(value)
        elif key.startswith(("model_file", "training_set", "eval_movie")) and value:
            variables[key] = Path(value)
    return variables


def write_json(config: Config, path: Path) -> None:
    """
    Write a configuration to a JSON file.

    This function transforms the ``Path``s to ``str``.

    Parameters
    ----------
    config : Config
        Configuration file to write.
    path : Path
        Path of the file to write.
    """
    LOGGER.info("Writing configuration to: %s", path)
    # Transform the Paths into strs

    with path.open(mode="w", encoding="utf8") as file:
        json.dump(config.sanitize(), file, indent=4)
